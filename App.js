import * as React from 'react';
import {
  Text,
  View,
  Platform,
  StyleSheet,
  TextInput,
  ScrollView,
  Button,
  Picker,
  Alert,
} from 'react-native';
import { Checkbox, RadioButton, Appbar } from 'react-native-paper';

// or any pure javascript modules available in npm

export default class App extends React.Component {
  state = {
    firstName: '',
    lastName: '',
    email: '',
    contact: '',
    department: '',
    Room: 'A',
    slot1: false,
    slot2: false,
    slot3: false,
    slot4: false,
    slot5: false,
  };

  handlefirstName = text => {
    this.setState({ firstName: text });
  };
  handlelastName = text => {
    this.setState({ lastName: text });
  };
  handleemail = text => {
    this.setState({ email: text });
  };
  handlecontact = text => {
    this.setState({ contact: text });
  };
  handledepartment = text => {
    this.setState({ department: text });
  };

  handleRoom = text => {
    this.setState({ Room: text });
  };
  handleslot1 = text => {
    this.setState({ slot1: text });
  };
  handleslot2 = text => {
    this.setState({ Room: text });
  };
  handleslot3 = text => {
    this.setState({ slot3: text });
  };
  handleslot4 = text => {
    this.setState({ slot4: text });
  };
  handleslot5 = text => {
    this.setState({ slot5: text });
  };

  /* listen = (
    firstName,
    lastName,
    email,
    department,
    Room,
    slot1,
    slot2,
    slot3,
    slot4,
    slot5
  ) => {
    alert(
      'firstName:' +
        firstName +
        '\nlastname:' +
        lastName +
        '\nemail:' +
        email +
        '\ndepartment:' +
        department +
        '\nRoom:' +
        Room +
        '\nslo1:' +
        slot1 +
        '\nslot2' +
        slot2 +
        slot3 +
        slot4 +
        slot5
    );
  };*/

  render() {
    const { Room } = this.state;
    const { slot1 } = this.state;
    const { slot2 } = this.state;
    const { slot3 } = this.state;
    const { slot4 } = this.state;
    const { slot5 } = this.state;

    return (
      <ScrollView>
        <View style={styles.container}>
          <Appbar.Header
            style={{ width: 370, backgroundColor: '#3d82f2', marginTop: -3 }}>
            <Appbar.Content title="Meeting Room Registration Form" />
          </Appbar.Header>

          <Text style={{ paddingTop: 10 }}>Contact Name</Text>

          <View style={{ flexDirection: 'row' }}>
            <View
              style={{ width: 180, height: 50, backgroundColor: 'skyblue' }}>
              <TextInput
                placeholder="firstName"
                autoCapitalize="none"
                placeholderTextColor="white"
                style={{ marginTop: 10, marginLeft: 10 }}
                onChangeText={this.handlefirstName}
              />
            </View>
            <View
              style={{ width: 175, height: 50, backgroundColor: 'skyblue' }}>
              <TextInput
                placeholder="lastName"
                autoCapitalize="none"
                placeholderTextColor="white"
                style={{ marginTop: 10, marginLeft: 10 }}
                onChangeText={this.handlelastName}
              />
            </View>
          </View>

          <Text style={{ paddingTop: 10 }}>Contact Email Address</Text>
          <View style={{ flexDirection: 'row' }}>
            <View
              style={{ width: 355, height: 50, backgroundColor: 'skyblue' }}>
              <TextInput
                placeholder="Email"
                autoCapitalize="none"
                placeholderTextColor="white"
                style={{ marginTop: 10, marginLeft: 10 }}
                onChangeText={this.handleemail}
              />
            </View>
          </View>
          <Text style={{ paddingTop: 10 }}>Contact Number</Text>
          <View style={{ flexDirection: 'row' }}>
            <View
              style={{ width: 355, height: 50, backgroundColor: 'skyblue' }}>
              <TextInput
                placeholder="Mobile Number"
                autoCapitalize="none"
                placeholderTextColor="white"
                style={{ marginTop: 10, marginLeft: 10 }}
                maxLength={10}
                onChangeText={this.handlecontact}
                keyboardType={'numeric'}
              />
            </View>
          </View>

          <Text style={{ paddingTop: 10 }}>Department</Text>
          <View style={{ flexDirection: 'row' }}>
            <View
              style={{ width: 355, height: 50, backgroundColor: 'skyblue' }}>
              <Picker
                selectedValue={this.state.department}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({ department: itemValue })
                }>
                <Picker.Item label="select" value="" />
                <Picker.Item label="CIVIL" value="CIVIL" />
                <Picker.Item label="CSE" value="CSE" />
                <Picker.Item label="ECE" value="ECE" />
                <Picker.Item label="EEE" value="EEE" />
                <Picker.Item label="ENGLISH" value="ENGLISH" />
                <Picker.Item label="IT" value="IT" />
                <Picker.Item label="MATHS" value="MATHS" />
                <Picker.Item label="MBA" value="MBA" />
                <Picker.Item label="MECH" value="MECH" />
              </Picker>
            </View>
          </View>

          <Text style={{ marginTop: 10, marginLeft: 10, paddingTop: 10 }}>
            Which room would you like to reserve ?
          </Text>
          <View>
            <RadioButton.Group
              onValueChange={Room => this.setState({ Room })}
              value={this.state.Room}>
              <View style={{ flexDirection: 'row' }}>
                <RadioButton
                  value="A"
                  status={Room === 'A' ? 'checked' : 'unchecked'}
                  onPress={() => {
                    this.setState({ Room: 'A' });
                  }}
                />
                <Text>Room A</Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <RadioButton
                  value="B"
                  status={Room === 'B' ? 'checked' : 'unchecked'}
                  onPress={() => {
                    this.setState({ Room: 'B' });
                  }}
                />
                <Text>Room B</Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <RadioButton
                  value="C"
                  status={Room === 'C' ? 'checked' : 'unchecked'}
                  onPress={() => {
                    this.setState({ Room: 'C' });
                  }}
                />
                <Text>Room c</Text>
              </View>
            </RadioButton.Group>
          </View>

          <Text style={{ marginTop: 10, marginLeft: 10 }}>
            Which time blocks would you like to reserve?
          </Text>
          <View>
            <View style={{ flexDirection: 'row' }}>
              <Checkbox
                status={slot1 ? 'checked' : 'unchecked'}
                onPress={() => {
                  this.setState({ slot1: !slot1 });
                }}
              />
              <Text>8 :00 - 10:00 a.m. </Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
              <Checkbox
                status={slot2 ? 'checked' : 'unchecked'}
                onPress={() => {
                  this.setState({ slot2: !slot2 });
                }}
              />
              <Text>11 :00 - 1:00 p.m. </Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
              <Checkbox
                status={slot3 ? 'checked' : 'unchecked'}
                onPress={() => {
                  this.setState({ slot3: !slot3 });
                }}
              />
              <Text>2 :00 - 4:00 p.m. </Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
              <Checkbox
                status={slot4 ? 'checked' : 'unchecked'}
                onPress={() => {
                  this.setState({ slot4: !slot4 });
                }}
              />
              <Text>5 :00 - 7:00 p.m. </Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
              <Checkbox
                status={slot5 ? 'checked' : 'unchecked'}
                onPress={() => {
                  this.setState({ slot5: !slot5 });
                }}
              />
              <Text>8 :00 - 10:00 p.m. </Text>
            </View>
          </View>

          <Button
            icon="add-a-photo"
            mode="contained"
            title="SUBMIT"
            /*  onPress={() =>
              this.listen(
                this.state.firstName,
                this.state.lastName,
                this.state.email,
                this.state.department,
                this.state.Room,
                this.state.slot1,
                this.state.slot2,
                this.state.slot3,
                this.state.slot4,
                this.state.slot5
              )
            }*/
            onPress={() =>
              fetch('http://172.17.4.181:3000/register1', {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                  firstName: this.state.firstName,
                  lastName: this.state.lastName,
                  email: this.state.email,
                  contact: this.state.contact,
                  department: this.state.department,
                  Room: this.state.Room,
                  slot1: this.state.slot1,
                  slot2: this.state.slot2,
                  slot3: this.state.slot3,
                  slot4: this.state.slot4,
                  slot5: this.state.slot5,
                }),
              })
                .then(res => Alert.alert('Bookings Successfull'))
                .catch(err => console.warn(err))
            }
          />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ecf0f1',
  },
});
